<?php

namespace App\Tests\Service\Auth;

use App\Service\Auth\GoogleService;
use PHPUnit\Framework\TestCase;

class GoogleServiceTest extends TestCase
{
    private $google;

    public function setUp()
    {
        $this->google = new GoogleService();
    }

    public function testValidationReturnsBoolean()
    {
        $bool = $this->google->validateIdToken('idToken', 'clientId');

        $this->assertIsBool($bool);
        $this->assertFalse($bool);
    }

    public function testGetUserInfoReturnsArray()
    {
        $array = $this->google->getUserInfo();

        $this->assertNull($array);
    }

    public function testServiceGivesExtId()
    {
        $id = $this->google->getExtId();

        $this->assertNull($id);
    }

    public function testServiceGivesEmail()
    {
        $email = $this->google->getEmail();

        $this->assertNull($email);
    }
}
