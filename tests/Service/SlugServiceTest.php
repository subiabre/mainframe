<?php

namespace App\Tests\Service;

use App\Service\SlugService;
use PHPUnit\Framework\TestCase;

class SlugServiceTest extends TestCase
{
    private $slug;

    public function setUp()
    {
        $this->slug = new SlugService();
    }

    public function testMakesFriendlySlug()
    {
        $slug1 = (string) $this->slug->make('Not an URL friendly string.');
        $slug2 = (string) $this->slug->make('V€ry w€ird%20string!');

        $this->assertEquals(
            'not-an-url-friendly-string', 
            $slug1
        );
        $this->assertEquals(
            'vry-wird20string',
            $slug2
        );
    }

    public function testInstanceToString()
    {
        $string = (string) $this->slug;

        $this->assertIsString($string);
    }
}
