<?php

namespace App\Tests\Service;

use App\Service\SnowflakeService;
use PHPUnit\Framework\TestCase;

class SnowflakeServiceTest extends TestCase
{
    private $snowflake;

    public function setUp()
    {
        $this->snowflake = new SnowflakeService();
    }

    public function testInstanceToString()
    {
        $string = (string) $this->snowflake;

        $this->assertIsString($string);
        $this->assertIsNumeric($string);
    }
}
