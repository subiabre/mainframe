<?php

namespace App\Tests\Service\Entity;

use App\Service\Entity\EntityService;
use PHPUnit\Framework\TestCase;

class EntityServiceTest extends TestCase
{
    private $service;

    public function setUp()
    {
        $entity = new \App\Entity\BaseEntity();

        $this->service = new EntityService();
        $this->service->setEntity($entity);
    }

    public function testServiceRetrievesInternalInstance()
    {
        $instance = $this->service->getEntity();

        $this->assertIsObject($instance);
        $this->assertInstanceOf(
            \App\Entity\BaseEntity::class, 
            $instance
        );
    }

    public function testServiceCanBeLoadedOnDemand()
    {
        $entity = new \App\Entity\UserEntity();
        $this->service->setEntity($entity);

        $instance = $this->service->getEntity();

        $this->assertInstanceOf(
            EntityService::class, 
            $this->service
        );

        $this->assertIsObject($instance);
        $this->assertInstanceOf(
            \App\Entity\UserEntity::class, 
            $instance
        );
    }

    public function testServiceGetsArrayWithAllPropertiesAndValues()
    {
        $array = $this->service->getProperties();

        $this->assertIsArray($array);
        $this->assertArrayHasKey('_id', $array);
    }

    public function testServiceGetsArrayIgnoringProperties()
    {
        $array = $this->service->getProperties(['_id']);

        $this->assertIsArray($array);
        $this->assertArrayNotHasKey('_id', $array);
    }

    public function testServiceMergesArray()
    {
        $user = new \App\Entity\UserEntity();
        $this->service->setEntity($user);

        $value = 'new name should be';
        $array = $this->service
            ->setProperties(['name' => $value])
            ->getProperties();
    
        $this->assertIsArray($array);
        $this->assertArrayHasKey('_id', $array);
        $this->assertEquals(
            $value,
            $array['name']
        );
    }

    public function testServiceMergesArrayIgnoringSystemProperties()
    {
        $user = new \App\Entity\UserEntity();
        $this->service->setEntity($user);

        $origin = $this->service->getProperties();

        $ignore = 'should be ignored';
        $value = 'should be accepted';

        $array = $this->service
            ->setProperties([
                '_id' => $ignore,
                'name' => $value
            ])->getProperties();

        $this->assertIsArray($array);
        $this->assertArrayHasKey('_id', $array);
        $this->assertNotEquals(
            $ignore,
            $array['_id']
        );
        $this->assertEquals(
            $origin['_id'],
            $array['_id']
        );
        $this->assertEquals(
            $value,
            $array['name']
        );
    }

    public function testServiceIgnoresArrayKeysThatDontExistInEntity()
    {
        $array = $this->service
            ->setProperties(['unexisting' => NULL])
            ->getProperties();

        $this->assertIsArray($array);
        $this->assertArrayNotHasKey('unexisting', $array);
    }

    public function testServiceCountsCountableProperties()
    {
        $user = new \App\Entity\UserEntity();
        $this->service->setEntity($user);
        
        $origin = $this->service->getProperties();

        $array = $this->service
            ->countProperties()
            ->getProperties();

        $this->assertIsArray($array);
        $this->assertArrayHasKey('postsCount', $array);
        $this->assertIsNumeric($array['postsCount']);
        $this->assertEquals(
            0,
            $array['postsCount']
        );
        $this->assertEquals(
            $origin['name'],
            $array['name']
        );
    }
}
