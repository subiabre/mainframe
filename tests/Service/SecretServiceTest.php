<?php

namespace App\Tests\Service;

use App\Service\SecretService;
use PHPUnit\Framework\TestCase;

class SecretServiceTest extends TestCase
{
    private $secret;

    public function setUp()
    {
        $this->secret = new SecretService();
    }

    public function testInstanceToString()
    {
        $string = (string) $this->secret;

        $this->assertIsString($string);
        $this->assertIsNotNumeric($string);
    }

    public function testRegeneration()
    {
        $secret1 = $this->secret->getNew();
        $secret2 = $this->secret->getNew();

        $this->assertNotEquals(
            $secret1,
            $secret2
        );
    }
}
