<?php

namespace App\Service;

use Symfony\Component\Dotenv\Dotenv;
use Godruoyi\Snowflake;

/**
 * Generates snowflake numerals to be used as ids.
 */
class SnowflakeService
{
    private $snowflakeObj;

    private $env;

    public function __construct()
    {
        $this->loadEnv();
        $this->loadSnowflake();
    }

    public function __toString()
    {
        return $this->new();
    }

    private function loadEnv()
    {
        $dotenv = new Dotenv();
        $dotenv->loadEnv(dirname(__DIR__, 2) . '/.env');

        $this->env['worker'] = $_ENV['WORKER_ID'] ?? 0;

        $this->env['datacenter'] = $_ENV['DATACENTER_ID'] ?? 0;
    }

    private function loadSnowflake()
    {
        $snowflake = new Snowflake\Snowflake(
            $this->env['datacenter'],
            $this->env['worker']
        );
        $sequenceResolver = new Snowflake\RandomSequenceResolver();

        $snowflake->setSequenceResolver($sequenceResolver);
        $snowflake->setStartTimeStamp(\strtotime('2000-01-01') * 1000);

        $this->snowflakeObj = $snowflake;
    }

    /**
     * Generates a new snowflake id
     * @return string
     */
    public function new(): string
    {
        return $this->snowflakeObj->id();
    }
}
