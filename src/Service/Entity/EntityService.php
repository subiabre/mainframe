<?php

namespace App\Service\Entity;

/**
 * General Entity Service
 */
class EntityService
{
    private $entity;

    /**
     * @param object $entity An Entity instance
     */
    public function __construct($entity = NULL)
    {
        $this->entity = $entity;
    }

    /**
     * Set the internal Entity
     * @param object $entity An Entity instance
     * @return self
     */
    public function setEntity($entity): self
    {
        $this->entity = $entity;

        return $this;
    }

    /**
     * Obtain the internal Entity instance
     * @return object
     */
    public function getEntity(): object
    {
        return $this->entity;
    }

    /**
     * Reads the internal class properties and returns a formated array
     * @return array
     */
    protected function readEntityProperties()
    {
        $reflect = new \ReflectionClass($this->entity);
        $props = $reflect->getProperties();

        $properties = [];

        foreach ($props as $prop) 
        {
            $prop->setAccessible(true);
            $properties[$prop->getName()] = $prop->getValue($this->entity); 
        }

        return $properties;
    }

    /**
     * Removes all the keys from an array that start with underscore
     * @param array $array Input array to be removed the underscore keys
     * @return array Input array without underscore keys
     */
    protected function removeSystemProperties(array $array): array
    {
        // Read keys in values and find the keys that start with underscore
        $keys = \array_keys($array);
        $system = \preg_grep('/[_]/', $keys);
        
        // Remove the underscore keys from the values
        foreach ($system as $key)
        {
            unset($array[$key]);
        } 

        return $array;
    }

    /**
     * Obtain an array of the Entity properties and values
     * @param array $ignore A list of the properties to be ignored from the return
     * @return array 
     */
    public function getProperties(array $ignore = []): array
    {
        $properties = $this->readEntityProperties();

        // Use the ignore values as keys for comparison
        if (\count($ignore) > 0)
        {
            foreach ($ignore as $key => $value)
            {
                $ignore[$value] = NULL;
            }

            // Return the different keys
            $properties = \array_diff_key($properties, $ignore);
        }

        return $properties;
    }

    /**
     * Fill an array with the missing Entity properties
     * @param array $properties Associative array of properties and their values
     * @return array
     */
    public function getMissingProperties(array $properties)
    {
        $entity = $this->readEntityProperties();

        // Fill the missing keys with their old values
        foreach ($entity as $key => $value)
        {
            if (!\array_key_exists($key, $properties))
            {
                $properties[$key] = $entity[$key];
            }
        }

        return $properties;
    }

    /**
     * Set the Entity properties with the given values \
     * Fills missing keys and ignores system properties
     * @param array $values Associative array of properties and values to be set
     * @return self
     */
    public function setProperties(array $values): self
    {
        $values = $this->removeSystemProperties($values);
        
        // Set each value using their respective method
        foreach ($values as $key => $value)
        {
            $method = 'set' . $key;
            if (\method_exists($this->entity, $method))
            {
                $this->entity->$method($value ?? '' || 0 || []); // Use default if NULL
            }
        }

        return $this;
    }

    /**
     * Calculate the number of items in each countable entity property
     * @return self
     */
    public function countProperties(): self
    {
        $properties = $this->readEntityProperties();
        // Read keys in values and find the keys that end with 'Count'
        $keys = \array_keys($properties);
        $countables = \preg_grep('/[*a-z]+(Count)/', $keys);

        // Count the array they reference to
        foreach ($countables as $count)
        {
            $countable = \rtrim($count, 'Count');
            $properties[$count] = \count($properties[$countable]);
        }

        $this->setProperties($properties);

        return $this;
    }
}
