<?php

namespace App\Service;

use Symfony\Component\Dotenv\Dotenv;

/**
 * Produce random strings
 */
class SecretService
{
    private $secret;

    private $env;

    public function __construct()
    {
        $this->loadEnv();
        $this->newSecret();
    }

    public function __toString()
    {
        return $this->get();
    }

    private function loadEnv()
    {
        $dotenv = new Dotenv();
        $dotenv->loadEnv(dirname(__DIR__, 2) . '/.env');

        $this->env['secret'] = $_ENV['APP_SECRET'] ?? '';
    }

    private function newSecret()
    {
        $pieces = [
            'salt' => $this->env['secret'],
            'time' => \microtime(true),
            'rand' => \random_bytes(32)
        ];

        $glue = '::';

        $this->secret = \hash('SHA256', \implode($glue, $pieces));
    }

    /**
     * Obtain the secret
     * @return string
     */
    public function get(): string
    {
        return $this->secret;
    }

    /**
     * Obtain a newly generated secret
     * @return string
     */
    public function getNew(): string
    {
        $this->newSecret();
        return $this->secret;
    }
}
