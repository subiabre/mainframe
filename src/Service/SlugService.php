<?php

namespace App\Service;

/**
 * Produce URL friendly string
 */
class SlugService
{
    private $slug = "";

    public function __toString()
    {
        return $this->getSlug();
    }

    /**
     * Transform an input string into an URL friendly value
     * @param string $input Input string
     * @param object $snowflake Id generating service
     * @return self
     */
    public function make($input, $snowflake = NULL)
    {
        // Remove non URL friendly characters
        $string = \preg_replace('/[^a-zA-Z0-9 ]/', '', $input);

        // Replace spaces with hyphens and make it lowercase
        $string = \preg_replace('/[ ]/', '-', $string);
        $string = \strtolower($string);

        // Encode snowflake in base 32 to save URL space
        if ($snowflake) {
            $snowflake = '-' . \base_convert((string) $snowflake, 10, 32);
        } else {
            $snowflake = '';
        }

        $this->slug = $string . $snowflake;

        return $this;
    }

    /**
     * Obtain the generated slug
     * @return string
     */
    public function getSlug(): string
    {
        return $this->slug;
    }
}

