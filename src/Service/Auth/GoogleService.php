<?php

namespace App\Service\Auth;

use Google_Client;

/**
 * Wrapper for Google APIs library: validate id tokens and get users info
 */
class GoogleService
{
    private $user;

    /**
     * Verify that the client has obtained a valid id token
     * @param string $idToken Google `id_token`
     * @param string $clientId Google `client_id`
     * @return bool
     */
    public function validateIdToken(?string $idToken, ?string $clientId): bool
    {
        $client = new Google_Client(['client_id' => $clientId]);

        try {
            $this->user = $client->verifyIdToken($idToken);

            if ($this->user) {
                return true;
            }

            return false;
        }
        
        catch (\UnexpectedValueException $error) {
            return false;
        }
    }

    /**
     * Obtain an user info array with the Google API response
     * @return array|null
     */
    public function getUserInfo(): ?array
    {
        return $this->user;
    }

    /**
     * Obtain the external id
     * @return string|null
     */
    public function getExtId(): ?string
    {
        return $this->user['sub'];
    }

    /**
     * Obtain the user email address
     * @return string|null
     */
    public function getEmail(): ?string
    {
        return $this->user['email'];
    }
}
