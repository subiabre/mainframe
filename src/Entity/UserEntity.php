<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\UserEntityRepository")
 */
class UserEntity extends BaseEntity
{
    /**
     * @ORM\Column(type="string", length=255)
     */
    private $_extId;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $_email;

    /**
     * @ORM\Column(type="string", length=15)
     */
    private $handle;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $description;

    /**
     * @ORM\Column(type="bigint", nullable=true)
     */
    private $picture;

    /**
     * @ORM\Column(type="array", nullable=true)
     */
    private $stories = [];

    /**
     * @ORM\Column(type="array", nullable=true)
     */
    private $storiesIn = [];

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $storiesCount;

    /**
     * @ORM\Column(type="array", nullable=true)
     */
    private $posts = [];

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $postsCount;

    /**
     * @ORM\Column(type="array", nullable=true)
     */
    private $comments = [];

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $commentsCount;

    /**
     * @ORM\Column(type="array", nullable=true)
     */
    private $reading = [];

    /**
     * @ORM\Column(type="bigint", nullable=true)
     */
    private $readingLast;

    /**
     * @ORM\Column(type="array", nullable=true)
     */
    private $starred = [];

    /**
     * @ORM\Column(type="array", nullable=true)
     */
    private $readed = [];

    /**
     * @ORM\Column(type="array", nullable=true)
     */
    private $readers = [];

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $readersCount;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $readedCount;

    public function getExtId(): ?string
    {
        return $this->_extId;
    }

    public function setExtId(string $_extId): self
    {
        $this->_extId = $_extId;

        return $this;
    }

    public function getEmail(): ?string
    {
        return $this->_email;
    }

    public function setEmail(string $_email): self
    {
        $this->_email = $_email;

        return $this;
    }

    public function getHandle(): ?string
    {
        return $this->handle;
    }

    public function setHandle(string $handle): self
    {
        $this->handle = $handle;

        return $this;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getPicture(): ?string
    {
        return $this->picture;
    }

    public function setPicture(?string $picture): self
    {
        $this->picture = $picture;

        return $this;
    }

    public function getStories(): ?array
    {
        return $this->stories;
    }

    public function setStories(?array $stories): self
    {
        $this->stories = $stories;

        return $this;
    }

    public function getStoriesIn(): ?array
    {
        return $this->storiesIn;
    }

    public function setStoriesIn(?array $storiesIn): self
    {
        $this->storiesIn = $storiesIn;

        return $this;
    }

    public function getStoriesCount(): ?int
    {
        return $this->storiesCount;
    }

    public function setStoriesCount(?int $storiesCount): self
    {
        $this->storiesCount = $storiesCount;

        return $this;
    }

    public function getPosts(): ?array
    {
        return $this->posts;
    }

    public function setPosts(?array $posts): self
    {
        $this->posts = $posts;

        return $this;
    }

    public function getPostsCount(): ?int
    {
        return $this->postsCount;
    }

    public function setPostsCount(?int $postsCount): self
    {
        $this->postsCount = $postsCount;

        return $this;
    }

    public function getComments(): ?array
    {
        return $this->comments;
    }

    public function setComments(?array $comments): self
    {
        $this->comments = $comments;

        return $this;
    }

    public function getCommentsCount(): ?int
    {
        return $this->commentsCount;
    }

    public function setCommentsCount(int $commentsCount): self
    {
        $this->commentsCount = $commentsCount;

        return $this;
    }

    public function getReading(): ?array
    {
        return $this->reading;
    }

    public function setReading(?array $reading): self
    {
        $this->reading = $reading;

        return $this;
    }

    public function getReadingLast(): ?string
    {
        return $this->readingLast;
    }

    public function setReadingLast(?string $readingLast): self
    {
        $this->readingLast = $readingLast;

        return $this;
    }

    public function getStarred(): ?array
    {
        return $this->starred;
    }

    public function setStarred(array $starred): self
    {
        $this->starred = $starred;

        return $this;
    }

    public function getReaded(): ?array
    {
        return $this->readed;
    }

    public function setReaded(?array $readed): self
    {
        $this->readed = $readed;

        return $this;
    }

    public function getReadersCount(): ?int
    {
        return $this->readersCount;
    }

    public function setReadersCount(?int $readersCount): self
    {
        $this->readersCount = $readersCount;

        return $this;
    }

    public function getReaders(): ?array
    {
        return $this->readers;
    }

    public function setReaders(?array $readers): self
    {
        $this->readers = $readers;

        return $this;
    }

    public function getReadedCount(): ?int
    {
        return $this->readedCount;
    }

    public function setReadedCount(?int $readedCount): self
    {
        $this->readedCount = $readedCount;

        return $this;
    }
}
