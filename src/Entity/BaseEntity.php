<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Base Entity with common properties and methods for every entity
 * @ORM\MappedSuperclass
 */
class BaseEntity
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue(strategy="NONE")
     * @ORM\Column(type="bigint")
     */
    protected $_id;

    /**
     * @ORM\Column(type="bigint")
     */
    protected $_timestamp;

    /**
     * @ORM\Column(type="bigint")
     */
    protected $_updated;

    public function getId(): ?string
    {
        return $this->_id;
    }

    public function setId(string $_id): self
    {
        $this->_id = $_id;

        return $this;
    }

    public function getTimestamp(): ?string
    {
        return $this->_timestamp;
    }

    public function setTimestamp(string $_timestamp): self
    {
        $this->_timestamp = $_timestamp;

        return $this;
    }

    public function getUpdated(): ?string
    {
        return $this->_updated;
    }

    public function setUpdated(string $_updated): self
    {
        $this->_updated = $_updated;

        return $this;
    }
}
