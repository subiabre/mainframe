<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Validator\Validator\ValidatorInterface;

/**
 * Base Controller with common methods for controllers
 */
class BaseController extends AbstractController
{
    private
        $validator,
        $validatorErrors = [],
        $validatorBool = false
        ;

    public function __construct(ValidatorInterface $validator)
    {
        $this->validator = $validator;
    }

    /**
     * Obtain the internal symfony validator instance
     * @return object
     */
    public function getValidator(): object
    {
        return $this->validator;
    }

    /**
     * Pass an entity instance to validate it's constraints
     * @param object $entity Instance to be validated
     * @return void
     */
    public function validateEntity(object $entity): void
    {
        $errors = $this->validator->validate($entity);

        if (\count($errors) > 0)
        {
            foreach ($errors as $error)
            {
                $this->validatorErrors[$error->getPropertyPath()] = $error->getMessage();
            }

            $this->validatorBool = true;
        }
    }

    /**
     * Get the array of errors after the validation
     * @return array
     */
    public function getValidatorErrors(): array
    {
        return $this->validatorErrors;
    }

    /**
     * Get the result of the validation as a boolean
     * @return bool
     */
    public function isValid(): bool
    {
        return $this->validatorBool;
    }
}
